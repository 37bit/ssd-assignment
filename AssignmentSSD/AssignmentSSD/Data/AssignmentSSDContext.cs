﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace AssignmentSSD.Models
{
    public class AssignmentSSDContext : DbContext
    {
        public AssignmentSSDContext (DbContextOptions<AssignmentSSDContext> options)
            : base(options)
        {
        }

        public DbSet<AssignmentSSD.Models.Games> Games { get; set; }
    }
}
