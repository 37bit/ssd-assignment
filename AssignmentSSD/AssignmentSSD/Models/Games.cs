﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssignmentSSD.Models
{
    public class Games
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public DateTime DatePublished { get; set; }
        public string Genre { get; set; }
        public decimal Price { get; set; }
    }
}
