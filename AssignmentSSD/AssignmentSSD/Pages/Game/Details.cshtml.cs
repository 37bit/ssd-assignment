﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using AssignmentSSD.Models;

namespace AssignmentSSD.Pages.Game
{
    public class DetailsModel : PageModel
    {
        private readonly AssignmentSSD.Models.AssignmentSSDContext _context;

        public DetailsModel(AssignmentSSD.Models.AssignmentSSDContext context)
        {
            _context = context;
        }

        public Games Games { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Games = await _context.Games.SingleOrDefaultAsync(m => m.ID == id);

            if (Games == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
