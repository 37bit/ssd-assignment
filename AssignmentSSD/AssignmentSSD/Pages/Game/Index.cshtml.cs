﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using AssignmentSSD.Models;

namespace AssignmentSSD.Pages.Game
{
    public class IndexModel : PageModel
    {
        private readonly AssignmentSSD.Models.AssignmentSSDContext _context;

        public IndexModel(AssignmentSSD.Models.AssignmentSSDContext context)
        {
            _context = context;
        }

        public IList<Games> Games { get;set; }

        public async Task OnGetAsync()
        {
            Games = await _context.Games.ToListAsync();
        }
    }
}
