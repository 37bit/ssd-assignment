﻿CREATE TABLE [dbo].[Games] (
    [ID]            INT             IDENTITY (1, 1) NOT NULL,
    [DatePublished] DATETIME2 (7)   NOT NULL,
    [Genre]         NVARCHAR (MAX)  NOT NULL,
    [Price]         DECIMAL (18, 2) NOT NULL,
    [Title]         NVARCHAR (MAX)  NOT NULL,
    CONSTRAINT [PK_Games] PRIMARY KEY CLUSTERED ([ID] ASC)
);

