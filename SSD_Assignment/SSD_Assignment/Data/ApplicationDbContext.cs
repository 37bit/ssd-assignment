using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SSD_Assignment.Models;
using SSD_Assignment.Data;

namespace SSD_Assignment.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<SSD_Assignment.Models.Games> Games { get; set; }

        public DbSet<SSD_Assignment.Models.ApplicationRole> ApplicationRole { get; set; }
        public DbSet<SSD_Assignment.Models.Feedback> Feedback { get; set; }
        public DbSet<AuditRecord> AuditRecords { get; set; }
        public DbSet<SSD_Assignment.Data.ApplicationUser> ApplicationUser { get; set; }

    }
}
