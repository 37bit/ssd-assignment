using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace SSD_Assignment.Data
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        [MaxLength(1000)]
        public string Description { get; set; }

        [DataType(DataType.PhoneNumber)]
        [MaxLength(8)]
        public override string PhoneNumber { get; set; }

        [DataType(DataType.EmailAddress)]
        [MaxLength(50)]
        public override string Email { get; set; }

    }
}
