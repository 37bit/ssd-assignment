﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SSD_Assignment.Models
{
    public class Feedback
    {
        public int FeedbackID { get; set; }

        public string feedback { get; set; }

        [Display(Name = "Posted By")]
        public string Username { get; set; }         
        //Logged in user performing the action

        [Display(Name = "Date/Time Stamp")]
        [DataType(DataType.DateTime)]
        public DateTime DateTimeStamp
        {   
            get; set;
        }
    }
}
