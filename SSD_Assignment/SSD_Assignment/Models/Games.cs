﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SSD_Assignment.Models
{
    public class Games
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }
        public decimal Price { get; set; }

        [Display(Name = "Date Published")]
        public DateTime DatePublished { get; set; }

        public string Description { get; set; }

        public string Gamefile { get; set; }

        [Display(Name="Game File Size (bytes)")]
        [DisplayFormat(DataFormatString = "{0:N1}")]
        public long GamefileSize { get; set; }
    }
}
