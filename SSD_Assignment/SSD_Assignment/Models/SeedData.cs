﻿using Microsoft.AspNetCore.Identity;
using SSD_Assignment.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSD_Assignment.Models
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager);

        }
        public static void SeedUsers(UserManager<ApplicationUser> userManager)
        {
            if (userManager.FindByNameAsync("JavierTang").Result == null)
            {
                ApplicationUser user = new ApplicationUser();
                user.UserName = "JavierTang";
                user.NormalizedUserName = "JAVIERTANG";
                user.Email = "javiertang@yahoo.com";
                user.NormalizedEmail = "JAVIERTANG@YAHOO.COM";
                user.SecurityStamp = Guid.NewGuid().ToString("D");
                user.EmailConfirmed = true;

                IdentityResult result = userManager.CreateAsync(user, "P@ssw0rd").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Admin").Wait();
                }
            }

            if (userManager.FindByNameAsync("YuChen").Result == null)
            {
                ApplicationUser user = new ApplicationUser();
                user.UserName = "YuChen";
                user.NormalizedUserName = "YUCHEN";
                user.Email = "yuchen@hotmail.com";
                user.NormalizedEmail = "YUCHEN@HOTMAIL.COM";
                user.SecurityStamp = Guid.NewGuid().ToString("D");
                user.EmailConfirmed = false;

                IdentityResult result = userManager.CreateAsync(user, "P@ssw0rd").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "User").Wait();
                }
            }

        }
        public static void SeedRoles(RoleManager<ApplicationRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync("User").Result)
            {
                ApplicationRole role = new ApplicationRole();
                role.Name = "User";
                role.Description = "User";
                role.NormalizedName = "USER";
                IdentityResult roleResult = roleManager.CreateAsync(role).Result;
            }
            if (!roleManager.RoleExistsAsync("Admin").Result)
            {
                ApplicationRole role = new ApplicationRole();
                role.Name = "Admin";
                role.Description = "Admin";
                role.NormalizedName = "ADMIN";
                IdentityResult roleResult = roleManager.CreateAsync(role).Result;
            }
        }
    }
}
