﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SSD_Assignment.Data;
using SSD_Assignment.Models;
using Microsoft.AspNetCore.Authorization;

namespace SSD_Assignment.Pages.Feedbacks
{
    [Authorize]
    public class CreateModel : PageModel
    {
        private readonly SSD_Assignment.Data.ApplicationDbContext _context;

        public CreateModel(SSD_Assignment.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            //throw new Exception("Test Error"); 
            return Page();
        }

        [BindProperty]
        public Feedback Feedback { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            //_context.Feedback.Add(Feedback);
            //await _context.SaveChangesAsync();   
            
            var userID = User.Identity.Name.ToString();
            Feedback.Username = userID;
            var feedback = new Feedback();
            Feedback.DateTimeStamp = DateTime.Now;

            _context.Feedback.Add(Feedback);

            //await _context.SaveChangesAsync();
            if (await _context.SaveChangesAsync() > 0)
            {
                // Create an auditrecord object                 
                var auditrecord = new AuditRecord();
                auditrecord.AuditActionType = "Add Feedback Record";
                auditrecord.DateTimeStamp = DateTime.Now;
                auditrecord.KeyfeedbackFieldID = Feedback.FeedbackID;

                // Get current logged-in use
                var user = User.Identity.Name.ToString();
                auditrecord.Username = user;

                _context.AuditRecords.Add(auditrecord);
                await _context.SaveChangesAsync();
            }


            return RedirectToPage("./Index");
        }
    }
}