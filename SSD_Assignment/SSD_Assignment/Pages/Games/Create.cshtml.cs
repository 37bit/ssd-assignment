﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SSD_Assignment.Data;
using SSD_Assignment.Models;
using SSD_Assignment.Utilities;
using System.IO;
using Microsoft.EntityFrameworkCore;
using System.Windows;
using System.Text;
using Microsoft.AspNetCore.Authorization;

namespace SSD_Assignment.Pages
{
    [Authorize(Roles="Admin")]
    public class CreateModel : PageModel
    {
        private readonly SSD_Assignment.Data.ApplicationDbContext _context;

        public CreateModel(SSD_Assignment.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public SSD_Assignment.Models.Games Games { get; set; }
        //public SSD_Assignment.Models.GameFileUpload GameFileUpload { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                
                return Page();
            }

            //var uploadGameData = await FileHelpers.ProcessFormFile(GameFileUpload.UploadGameFile, ModelState);

            //var filePath =@"C:\temp";
            
            //using (var filestream = new FileStream(filePath, FileMode.Create))
            //{
            //    await GameFileUpload.UploadGameFile.CopyToAsync(filestream);
            //}
            
            _context.Games.Add(Games);
            //await _context.SaveChangesAsync();

            // Once a record is added, create an audit record             
            if (await _context.SaveChangesAsync()>0)
            {
                // Create an auditrecord object                 
                var auditrecord = new AuditRecord();
                auditrecord.AuditActionType = "Add Game Record";
                auditrecord.DateTimeStamp = DateTime.Now;
                auditrecord.KeyGameFieldID = Games.ID;

                // Get current logged-in use
                var userID = User.Identity.Name.ToString();
                auditrecord.Username = userID;

                _context.AuditRecords.Add(auditrecord);
                await _context.SaveChangesAsync();
            }   
            return RedirectToPage("./Index");
        }
    }
}