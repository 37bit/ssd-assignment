﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SSD_Assignment.Data;
using SSD_Assignment.Models;

namespace SSD_Assignment.Pages
{
    public class IndexModel : PageModel
    {
        private readonly SSD_Assignment.Data.ApplicationDbContext _context;

        public IndexModel(SSD_Assignment.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<SSD_Assignment.Models.Games> Games { get;set; }
        public SelectList Genres { get; set; }
        public string GamesGenre { get; set; }

        public async Task OnGetAsync(string gamesGenre, string searchString)
        {
            IQueryable<string> genreQuery = from g in _context.Games
                                            orderby g.Genre
                                            select g.Genre;

            var games = from g in _context.Games
                        select g;

            if (!String.IsNullOrEmpty(searchString))
            {
                games = games.Where(s => s.Name.Contains(searchString));
            }
            if (!String.IsNullOrEmpty(gamesGenre))
            {
                games = games.Where(x => x.Genre == gamesGenre);
            }

            Genres = new SelectList(await genreQuery.Distinct().ToListAsync());
            Games = await _context.Games.ToListAsync();

        }
    }
}
